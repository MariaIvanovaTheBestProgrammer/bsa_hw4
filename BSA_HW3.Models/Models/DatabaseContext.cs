﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace BSA_HW3.Data.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options) { }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var users = new List<User>
            {
                new User {
                    Id = 1, 
                    FirstName = "Maria", 
                    LastName = "Ivanova", 
                    Email = "maria.nova.865@gmail.com",
                    RegisteredAt = new DateTime(2020, 02, 26),
                    BirthDay = new DateTime(2001, 05, 03),
                    TeamId = 1
                },
                new User {
                    Id = 2,
                    FirstName = "Ruslan",
                    LastName = "Voskresenskiy",
                    Email = "vos_nau@gmail.com",
                    RegisteredAt = new DateTime(2020, 06, 15),
                    BirthDay = new DateTime(2001, 05, 07),
                    TeamId = 1
                },
                new User {
                    Id = 3,
                    FirstName = "Valeria",
                    LastName = "Nazarenko",
                    Email = "lero4ka@gmail.com",
                    RegisteredAt = new DateTime(2019, 10, 08),
                    BirthDay = new DateTime(2001, 07, 05),
                    TeamId = 1
                },
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    TaskId = 1,
                    TaskName = "ModelCreating",
                    TaskState = TaskState.ToDo,
                    Description = "",
                    PerformerId = 1,
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 11, 03)
                },
                new Task
                {
                    TaskId = 2,
                    TaskName = "Logic",
                    TaskState = TaskState.Done,
                    Description = "",
                    PerformerId = 2,
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 11, 03),
                    FinishedAt = new DateTime(2021, 01, 15)
                },
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Bubochki",
                    CreatedAt = new DateTime(2020, 02, 12)
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    TeamId = 1,
                    UserId = 3,
                    ProjectName = "Bubenchiki",
                    Descriprion = "",
                    CreatedAt = new DateTime(2019, 11, 03),
                }
            };

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);

            base.OnModelCreating(modelBuilder);
        }
    }
}
