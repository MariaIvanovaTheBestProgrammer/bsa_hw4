﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BSA_HW3.Data.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
    [Table("Task")]
    public class Task
    {
        [Required]
        public int TaskId { get; set; }

        [Required]
        public int ProjectId { get; set; }

        public Project Project { get; set; }

        [Required]
        public int PerformerId { get; set; }

        public User Performer { get; set; }

        [Required]
        [StringLength(20)]
        public string TaskName { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Required]
        public TaskState TaskState { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }

    }
}
