﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BSA_HW3.Data.Models
{
    [Table("User")]
    public class User
    {
        [Required]
        public int Id { get; set; }

        public int? TeamId { get; set; }

        public Team Team { get; set; }

        [Required]
        [StringLength(10)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")]
        public string Email { get; set; }

        [Required]
        public DateTime RegisteredAt { get; set; }

        [Required]
        public DateTime BirthDay { get; set; }

        public IEnumerable<Task> Tasks { get; set; }

        public IEnumerable<Project> Projects { get; set; }
    }
}
