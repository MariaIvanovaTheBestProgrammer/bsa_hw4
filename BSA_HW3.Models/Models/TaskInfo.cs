﻿
namespace BSA_HW3.Data.Models
{
    public class TaskInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
