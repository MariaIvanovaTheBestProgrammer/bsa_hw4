﻿using System.Collections.Generic;

namespace BSA_HW3.Data.Models
{
    public class TeamInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
