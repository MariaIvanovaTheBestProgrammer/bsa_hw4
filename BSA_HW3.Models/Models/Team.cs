﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BSA_HW3.Data.Models
{
    [Table("Team")]
    public class Team
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string TeamName { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}
