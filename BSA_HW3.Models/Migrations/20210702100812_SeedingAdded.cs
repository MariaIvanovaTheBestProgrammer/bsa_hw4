﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA_HW3.Data.Migrations
{
    public partial class SeedingAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Team",
                columns: new[] { "Id", "CreatedAt", "TeamName" },
                values: new object[] { 1, new DateTime(2020, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bubochki" });

            migrationBuilder.InsertData(
                table: "Project",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Descriprion", "ProjectName", "TeamId", "UserId" },
                values: new object[] { 1, 3, new DateTime(2019, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "", "Bubenchiki", 1, null });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2001, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "maria.nova.865@gmail.com", "Maria", "Ivanova", new DateTime(2020, 2, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, new DateTime(2001, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "vos_nau@gmail.com", "Ruslan", "Voskresenskiy", new DateTime(2020, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 3, new DateTime(2001, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "lero4ka@gmail.com", "Valeria", "Nazarenko", new DateTime(2019, 10, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 }
                });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "TaskId", "CreatedAt", "Description", "FinishedAt", "PerformerId", "PermormerId", "ProjectId", "TaskName", "TaskState" },
                values: new object[] { 1, new DateTime(2020, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "", null, 1, null, 1, "ModelCreating", 0 });

            migrationBuilder.InsertData(
                table: "Task",
                columns: new[] { "TaskId", "CreatedAt", "Description", "FinishedAt", "PerformerId", "PermormerId", "ProjectId", "TaskName", "TaskState" },
                values: new object[] { 2, new DateTime(2020, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "", new DateTime(2021, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, null, 1, "Logic", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Task",
                keyColumn: "TaskId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Task",
                keyColumn: "TaskId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Project",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Team",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
