﻿using System.Collections.Generic;
using BSA_HW3.Data.Models;

namespace BSA_HW3.Data.Interfaces
{
    public interface ITaskRepository
    {
        IEnumerable<Task> GetTasks();
        Task GetTaskById(int taskId);
        void CreateTask(Task task);
        void DeleteTask(int taskId);
        void UpdateTask(Task task);
    }
}
