﻿using AutoMapper;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.Data.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly DatabaseContext _context;

        public TeamRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void CreateTeam(Team team)
        {
            _context.Add(team);
            _context.SaveChanges();
        }

        public void DeleteTeam(int teamId)
        {
            var entity = _context.Teams.Where(e => e.Id == teamId).FirstOrDefault();
            _context.Remove(entity);
            _context.SaveChanges();
        }


        public Team GetTeamById(int teamId)
        {
            return _context.Teams.Where(e => e.Id == teamId).FirstOrDefault();
        }

        public IEnumerable<Team> GetTeams()
        {
            return _context.Teams;
        }

        public void UpdateTeam(Team team)
        {
            var tmp = _context.Teams.FirstOrDefault(e => e.Id == team.Id);
            if (tmp != null)
            {
                tmp.TeamName = team.TeamName;
                tmp.CreatedAt = team.CreatedAt;
            }
            _context.SaveChanges();
        }

    }
}
