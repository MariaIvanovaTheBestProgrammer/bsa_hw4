﻿using BSA_HW3.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BSA_HW3.Data.Models;

namespace BSA_HW3.Data.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly DatabaseContext _context;

        public ProjectRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void CreateProject(Project project)
        {
            _context.Add(project);
            _context.SaveChanges();
        }

        public void DeleteProject(int projectId)
        {
            var entity = _context.Projects.Where(p => p.Id == projectId).FirstOrDefault();
            _context.Remove(entity);
            _context.SaveChanges();
        }

        public Project GetProjectById(int projectId)
        {
            return _context.Projects.Where(e => e.Id == projectId).FirstOrDefault();
        }

        public IEnumerable<Project> GetProjects()
        {
            return _context.Projects;
        }

        public void UpdateProject(Project project)
        {
            var tmp = _context.Projects.FirstOrDefault(e => e.Id == project.Id);
            if (tmp != null)
            {
                tmp.ProjectName = project.ProjectName;
                tmp.TeamId = project.TeamId;
                tmp.Team = project.Team;
                tmp.UserId = project.UserId;
                tmp.User = project.User;
                tmp.CreatedAt = project.CreatedAt;
                tmp.Deadline = project.Deadline;
                tmp.Descriprion = project.Descriprion;
            }
            _context.SaveChanges();
        }

    }

}
