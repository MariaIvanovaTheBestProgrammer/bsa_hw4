﻿using AutoMapper;
using BSA_HW3.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BSA_HW3.Data.Models;

namespace BSA_HW3.Data.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly DatabaseContext _context;

        public TaskRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void CreateTask(Task task)
        {
            _context.Add(task);
            _context.SaveChanges();
        }

        public void DeleteTask(int taskId)
        {
            var entity = _context.Tasks.Where(p => p.TaskId == taskId).FirstOrDefault();
            _context.Remove(entity);
            _context.SaveChanges();
        }

        public Task GetTaskById(int taskId)
        {
            return _context.Tasks.Where(e => e.TaskId == taskId).FirstOrDefault();
        }

        public IEnumerable<Task> GetTasks()
        {
            return _context.Tasks;
        }

        public void UpdateTask(Task task)
        {
            var tmp = _context.Tasks.FirstOrDefault(e => e.TaskId == task.TaskId);
            if (tmp != null)
            {
                tmp.TaskName = task.TaskName;
                tmp.PerformerId = task.PerformerId;
                tmp.Performer = task.Performer;
                tmp.ProjectId = task.ProjectId;
                tmp.Project = task.Project;
                tmp.TaskState = task.TaskState;
                tmp.CreatedAt = task.CreatedAt;
                tmp.FinishedAt = task.FinishedAt;
                tmp.Description = task.Description;
            }
            _context.SaveChanges();
        }

    }
}
