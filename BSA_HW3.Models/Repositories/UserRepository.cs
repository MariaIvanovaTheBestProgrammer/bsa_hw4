﻿using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DatabaseContext _context;

        public UserRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void CreateUser(User user)
        {
            _context.Add(user);
            _context.SaveChanges();
        }

        public void DeleteUser(int userId)
        {
            var entity = _context.Users.Where(e => e.Id == userId).FirstOrDefault();
            _context.Remove(entity);
            _context.SaveChanges();
        }

        public User GetUserById(int userId)
        {
            return _context.Users.Where(e => e.Id == userId).FirstOrDefault();
        }

        public IEnumerable<User> GetUsers()
        {
            return _context.Users;
        }

        public void UpdateUser(User user)
        {
            var tmp = _context.Users.FirstOrDefault(e => e.Id == user.Id);
            if (tmp != null)
            {
                tmp.FirstName = user.FirstName;
                tmp.LastName = user.LastName;
                tmp.TeamId = user.TeamId;
                tmp.Team = user.Team;
                tmp.Email = user.Email;
                tmp.BirthDay = user.BirthDay;
            }
            _context.SaveChanges();
        }
    }
}
