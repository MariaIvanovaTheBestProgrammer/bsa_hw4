﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet("tasks")]
        public ActionResult<IEnumerable<TaskDTO>> GetTasks()
        {
            return Ok(_taskService.GetTasks());
        }
        [HttpGet("{taskId}")]
        public ActionResult<TaskDTO> GetTaskById(int taskId)
        {
            return Ok(_taskService.GetTaskById(taskId));
        }
        [HttpDelete("{taskId}")]
        public IActionResult DeleteTask(int taskId)
        {
            _taskService.DeleteTask(taskId);
            return NoContent();
        }
        [HttpPost]
        public IActionResult CreateTask(TaskDTO taskDTO)
        {
            _taskService.CreateTask(taskDTO);
            return StatusCode(201, _taskService.GetTaskById(taskDTO.TaskId));
        }
        [HttpPut]
        public IActionResult UpdateTask(TaskDTO taskDTO)
        {
            _taskService.UpdateTask(taskDTO);
            return Ok(_taskService.GetTaskById(taskDTO.TaskId));
        }
        [HttpGet("UserTasks")]
        public ActionResult<IEnumerable<TaskDTO>> GetUserTasks(int userId)
        {
            return Ok(_taskService.GetUserTasks(userId));
        }
        [HttpGet("FinishedTasks")]
        public ActionResult<IEnumerable<TaskInfo>> GetFinishedTaskIdName(int userId)
        {
            return Ok(_taskService.GetFinishedTaskIdName(userId));
        }

    }
}
