﻿using System.Threading.Tasks;

namespace BSA_HW3.Menu
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Menu menu = new Menu();
            int op;
            do
            {
                op = menu.menu();
                if (op == 1)
                {
                    await menu.AddProject();
                }
                if (op == 2)
                {
                    await menu.DisplayAllProjects();
                }
                if (op == 3)
                {
                    await menu.DisplayProjectById();
                }
                if (op == 4)
                {
                    await menu.UpdateProject();
                }
                if (op == 5)
                {
                    await menu.DeleteProject();
                }
                if (op == 6)
                {
                    await menu.DisplayProjectToTasksCountDictionary();
                }
                if (op == 7)
                {
                    await menu.DisplayProjectInfo();
                }
                if (op == 8)
                {
                    await menu.AddTask();
                }
                if (op == 9)
                {
                    await menu.DisplayAllTasks();
                }
                if (op == 10)
                {
                    await menu.DisplayTaskById();
                }
                if (op == 11)
                {
                    await menu.UpdateTask();
                }
                if (op == 12)
                {
                    await menu.DeleteTask();
                }
                if (op == 13)
                {
                    await menu.DisplayUserTasks();
                }
                if (op == 14)
                {
                    await menu.DisplayFinishedTaskIdName();
                }
                if (op == 15)
                {
                    await menu.AddTeam();
                }
                if (op == 16)
                {
                    await menu.DisplayAllTeams();
                }
                if (op == 17)
                {
                    await menu.DisplayTeamById();
                }
                if (op == 18)
                {
                    await menu.UpdateTeam();
                }
                if (op == 19)
                {
                    await menu.DeleteTeam();
                }
                if (op == 20)
                {
                    await menu.DisplaySortedTeamsInfo();
                }
                if (op == 21)
                {
                    await menu.AddUser();
                }
                if (op == 22)
                {
                    await menu.DisplayAllUsers();
                }
                if (op == 23)
                {
                    await menu.DisplayUserById();
                }
                if (op == 24)
                {
                    await menu.UpdateUser();
                }
                if (op == 25)
                {
                    await menu.DeleteUser();
                }
                if (op == 26)
                {
                    await menu.DisplayUsersSortedByName();
                }
                if (op == 27)
                {
                    await menu.DisplayUserProjectInfo();
                }
            } while (op != 99);
        }
    }
}
