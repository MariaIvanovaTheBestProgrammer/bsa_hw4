﻿using AutoMapper;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;

namespace BSA_HW3.BusinessLogic.Profiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<TaskDTO, Task>()
                .ForMember(dest => dest.Project, opt => opt.Ignore());
        }
    }
}
